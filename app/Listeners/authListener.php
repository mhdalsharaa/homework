<?php

namespace App\Listeners;

use App\Events\authEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Http;

class authListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(authEvent $event): void
    {
        $response = Http::post('https://jsonplaceholder.typicode.com/posts', [
            'otp' => $event->otp,
        ]);
    }
}
