<?php

namespace App\Http\Controllers\Api;

use App\Events\authEvent;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckOtpRequest;
use App\Http\Requests\StoreOtpRequest;
use App\Http\Requests\StoreUserRequest;

class UserController extends Controller
{
    public function __construct(protected UserService $userService)
    {
    }
    public function loginAttempt(StoreOtpRequest $request)
    {
        $data = $request->validated();
        $otp = $this->userService->loginAttempt($data);
        return response()->json(['message' => 'Check your inbox for the OTP' . $otp], 200);
    }
    public function login(CheckOtpRequest $request)
    {
        $data = $request->validated();
        $user = $this->userService->login($data);
        event(new authEvent($user));
        return response()->json(['message' => 'Login successful'], 200);
    }
    public function register(StoreUserRequest $request)
    {
        $data = $request->validated();
    }
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json(['message' => 'Logout successful']);
    }
}
