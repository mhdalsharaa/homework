<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\GetCourseRequest;
use App\Http\Requests\StorePackageRequest;
use App\Http\Resources\PackageResource;
use App\Models\Package;
use App\Services\PackageService;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    public function __construct(protected PackageService $packageService)
    {
    }
    public function index(GetCourseRequest $request)
    {
        $data = $request->validated();
        $package = $this->packageService->index($data);
        return response()->json(new PackageResource($package));
    }
    public function store(StorePackageRequest $request)
    {
        $data = $request->validated();
        $package = $this->packageService->store($data);
        return response()->json(new packageResource($package));
    }
    public function show($id)
    {
        $package = Package::findOrFail($id);
        return response()->json(new PackageResource($package));
    }
    public function destroy($id)
    {
        Package::findOrFail($id)->delete();
        return response()->json(['message' => 'deleted Successfully']);
    }
}
