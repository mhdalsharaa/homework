<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\GetCourseRequest;
use App\Http\Requests\getPackageRequest;
use App\Http\Requests\StoreCourseRequest;
use App\Http\Requests\StorePackageRequest;
use App\Http\Resources\CourseResource;
use App\Models\Course;
use App\Services\CourseService;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function __construct(protected CourseService $courseService)
    {
    }
    public function index(getPackageRequest $request)
    {
        $data = $request->validated();
        $course = $this->courseService->index($data);
        return response()->json(new CourseResource($course));
    }
    public function store(StorePackageRequest $request)
    {
        $data = $request->validated();
        $course = $this->courseService->store($data);
        return response()->json(new CourseResource($course));
    }
    public function show($id)
    {
        $course = Course::findOrFail($id);
        return response()->json(new CourseResource($course));
    }
    public function destroy($id)
    {
        Course::findOrFail($id)->delete();
        return response()->json(['message' => 'deleted Successfully']);
    }
}
