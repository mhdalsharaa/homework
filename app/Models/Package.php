<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['name', 'price'];
    public function courses()
    {
        return $this->hasMany(Course::class);
    }
    public function Purchases()
    {
        return $this->hasMany(Purchase::class);
    }
    public function imageable()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
