<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['name', 'price', 'package_id'];
    public function package()
    {
        return $this->belongsTo(Package::class);
    }
    public function Purchases()
    {
        return $this->hasMany(Purchase::class);
    }
    public function imageable()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
