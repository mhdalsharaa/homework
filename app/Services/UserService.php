<?php

namespace App\Services;

use App\Models\OtpAttempt;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserService
{
    public function signup($data)
    {
        $user = User::create([
            'email' => $data['email'],
        ]);


        return response()->json(['message' => 'User registered successfully', 'user' => $user], 201);
    }
    public function loginAttempt($data)
    {

        $user = User::where('email', $data['email'])->first();

        // Generate OTP (assuming it's a 6-digit number)
        $otp = mt_rand(100000, 999999);

        $expireAt = Carbon::now()->addMinutes(30);
        // Save OTP attempt in the database
        $otpAttempt = OtpAttempt::create([
            'otp' => $otp,
            'user_id' => $user->id,
            'expire_at' => $expireAt,
        ]);
        return $otp;
    }
    public function login($data)
    {
        $user = User::where('email', $data['email'])->first();

        // Check if there is a valid OTP attempt for the user
        $otpAttempt = OtpAttempt::where('user_id', $user->id)
            ->where('otp', $data['otp'])
            ->where('expire_at', '>', Carbon::now())
            ->first();

        if (!$otpAttempt) {
            return response()->json(['error' => 'Invalid OTP or OTP expired'], 401);
        }
        // Optionally, you can clear the used OTP attempt
        $otpAttempt->delete();

        return response()->json(['message' => 'Login successful'], 200);
    }
}
