<?php

namespace App\Services;

use App\Models\Course;

class CourseService
{
    public function index($data)
    {
        $course = Course::query();

        $course->when(request()->filled('search'), function ($query) {
            $search = request()->search;
            $query->where("name", 'LIKE', '%' . $search . '%');
        });
        $course = Course::all();
        return $course;
    }
    public function store($data)
    {
        $course = Course::updateOrCreate(['id' => $data['course_id'] ?? null], $data);

        if (isset($data['image'])) {

            $imagePath = $this->uploadImage($data['image'], $course->id);
            $course->imageable()->delete();
            $course->imageable()->create([
                'path' => $imagePath,
                'image_type' => 'Category',
            ]);
        }
        return $course;
    }
    protected function uploadImage($image, $courseId)
    {

        $image->store("public/Images/Category/$courseId");
        return "storage/Images/Category/$courseId/" . $image->hashName();
    }
}
