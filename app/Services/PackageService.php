<?php

namespace App\Services;

use App\Models\Package;

class PackageService
{
    public function index($data)
    {
        $package = Package::query();

        $package->when(request()->filled('search'), function ($query) {
            $search = request()->search;
            $query->where("name", 'LIKE', '%' . $search . '%');
        });
        $package = Package::all();
        return $package;
    }
    public function store($data)
    {
        $package = Package::updateOrCreate(['id' => $data['Package_id'] ?? null], $data);

        if (isset($data['image'])) {

            $imagePath = $this->uploadImage($data['image'], $package->id);
            $package->imageable()->delete();
            $package->imageable()->create([
                'path' => $imagePath,
                'image_type' => 'Category',
            ]);
        }
        return $package;
    }
    protected function uploadImage($image, $packageId)
    {

        $image->store("public/Images/Category/$packageId");
        return "storage/Images/Category/$packageId/" . $image->hashName();
    }
}
